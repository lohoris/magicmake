#!/usr/bin/env python3

"""
	MagicMake, (c) 2012-2013 Lorenzo Petrone
	
	Copying and distribution of this file, with or without modification,
	are permitted in any medium without royalty provided the copyright
	notice and this notice are preserved.  This file is offered as-is,
	without any warranty.
"""

###### includes

import os
import re
import sys
from pprint import pprint


###### functions

def walk_sources (dirs_source):
	sources=[]
	headers=[]
	
	for path in dirs_source:
		for root, dirs, files in os.walk(path):
			for file in files:
				name,ext=os.path.splitext(file)
				if root[:2]=="./":
					rt=root[2:] # removes leading ./
				else:
					rt=root
				
				if ext=='.c' or ext=='.cpp':
					sources.append((rt+'/',file))
				elif ext=='.h' or ext=='.hpp':
					headers.append((rt+'/',file))
	
	return (sources,headers)

def depends (list, main=None):
	if main!=None:
		main=()
	depends={}
	for elem in list:
		(root,name)=elem
		
		nline=0
		with open(root+name, encoding='utf-8') as file:
			depends[name]=[]
			if OPT["VERBOSE"]>0:
				print("\tchecking depends: "+name)
			
			for line in file:
				nline=nline+1
				if OPT["VERBOSE"]>1:
					print("\t\tchecking line "+str(nline))
				
				if main==():
					if line.startswith('int main (') or line.startswith('void main ('):
						main=name
				
				# checks for included files
				if not line.startswith('#include "'):
					continue
				inc=line.split('"')[1] # file being included
				
				depends[name].append(inc)
	
	if main==None:
		return depends
	return (depends,main)

def select_compiler (name):
	xname,ext=os.path.splitext(name)
	if ext=='.cpp':
		return 'g++'
	return 'gcc'

def chose_execname (name):
	return name.split('.')[:1][0]

def chose_oname (name):
	return name.split('.')[:1][0]+'.o'

def add_main (name, search_file, bdir, odir, source_depends, header_depends, linkto):
	ret=''
	
	execname=bdir+chose_execname(name)
	
	# depends
	deps=set()
	for dep1 in source_depends[main]:
		deps=add_depends(dep1,search_file,header_depends,deps)
		for dep2 in header_depends[dep1]:
			deps=add_depends(dep2,search_file,header_depends,deps)
	
	# line above
	
	ret=ret+execname+": "+search_file[name]+name
	
	for dep in deps:
		fullname=search_file[dep]+dep
		ret=ret+" "+fullname
	
	for source in source_depends:
		if source==main:
			continue
		ret=ret+" "+odir+chose_oname(source)
	
	# line below
	
	ret=ret+"\n"
	
	ret=ret+"\t"+'$(CC) $(CFLAGS) '+search_file[name]+name
	for source in source_depends:
		if source==main:
			continue
		ret=ret+" "+odir+chose_oname(source)
	ret=ret+" -o "+execname
	ret=ret+" "+linkto
	
	ret=ret+"\n"
	ret=ret+"\n"
	
	return ret

def add_source (name, search_file, odir, source_depends, header_depends):
	ret=''
	
	oname=odir+chose_oname(name)
	
	# depends
	deps=set()
	for dep in source_depends[source]:
		deps=add_depends(dep,search_file,header_depends,deps)
	
	# line above
	ret=ret+oname+": "+search_file[name]+name
	for dep in deps:
		fullname=search_file[dep]+dep
		ret=ret+" "+fullname
	
	# line below
	ret=ret+"\n"
	ret=ret+"\t"+'$(CC) $(CFLAGS) -c '+search_file[name]+name+" -o "+oname
	ret=ret+"\n"
	ret=ret+"\n"
	
	return ret

def add_depends (name, search_file, header_depends, deps):
	if name in deps:
		return deps
	deps.add(name)
	for hdep in header_depends[name]:
		deps=add_depends(hdep,search_file,header_depends,deps)
	return deps

def search_files (sources):
	ret={}
	for path,name in sources:
		ret[name]=path
	return ret



###### main

print("Magicmake: analyzing sources...")

# command-line options

dirs=['.']
for ext in sys.argv[1:]:
	dirs.append(ext)

# gets the options
# TODO. accept command line options

OPT={}
OPT["CC"]=""
OPT["CFLAGS"]=""
OPT["LINKTO"]=""
OPT["VERBOSE"]=0
try:
	with open("Magicmake") as file:
		for line in file:
			(com,val)=line.split('=',1)
			com=re.sub(r'\s','',com)
			OPT[com]=val # TODO. it would be cool to remove leading whitespace from val
except IOError:
	None # TODO. would be nicer to check if file exists instead
if not isinstance(OPT["VERBOSE"],int):
	OPT["VERBOSE"]=int(OPT["VERBOSE"])

# TODO. maybe we could make these configurable too
bdir="bin/"
odir="o/"

# searches for files and their dependencies

(sources,headers)=walk_sources(dirs)
search_file=search_files(sources+headers)
(source_depends,main)=depends(sources,True)
(header_depends)=depends(headers)

# fixes some options

if OPT["CC"]=="":
	OPT["CC"]=select_compiler(main)


#### creates the Makefile

## variables

Makefile=""
Makefile=Makefile+"CC = "+OPT["CC"]+"\n"
Makefile=Makefile+"CFLAGS = "+OPT["CFLAGS"]+"\n"

## main
if OPT["VERBOSE"]>0:
	print("\tadding main")
Makefile=Makefile+add_main(main,search_file,bdir,odir,source_depends,header_depends,OPT["LINKTO"])

## .o
for source in source_depends:
	if source==main:
		continue
	if OPT["VERBOSE"]>0:
		print("\tadding: "+source)
	Makefile=Makefile+add_source(source,search_file,odir,source_depends,header_depends)

## "footer"

Makefile=Makefile+".PHONY: clean\nclean:\n\t-rm %s* %s*\n\n"%(bdir,odir)
Makefile=Makefile+".PHONY: cleanr\ncleanr:\n\t-rm -rf %s* %s*\n"%(bdir,odir)

## writes to disk
with open("Makefile",'w') as file:
	file.write(Makefile)

print("Magicmake: Makefile written to disk.")
